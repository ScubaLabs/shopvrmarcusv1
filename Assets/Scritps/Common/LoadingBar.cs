﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {

	#region Script's Summary
	//This script is controlling and displaying the LoadingBar image
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private GameObject GUIReticle;
	[SerializeField] private CameraScene1 CameraScene1_Script; 

	private Image self_Image_Component;

	private float fill_Time;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	/*
	void Awake (){

	}
	*/

	void Start () {

		//--------- variable definitions -----------

		self_Image_Component = GetComponent<Image> ();

		fill_Time = 1.5f;

		//-------------------------------------------
	}
		
	void Update () {

		if (self_Image_Component.fillAmount != 1) {
			self_Image_Component.fillAmount += (Time.deltaTime / fill_Time);
		} else {
			self_Image_Component.fillAmount = 0;
		}
	}

	void OnEnable(){

		GUIReticle.SetActive(false);
		CameraScene1_Script.enabled = false;
	}

	void OnDisable(){

		GUIReticle.SetActive(true);
		CameraScene1_Script.enabled = true;
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------



	//----------------------------------------------------------------------

	#endregion
}
