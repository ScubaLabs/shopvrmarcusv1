﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipText : MonoBehaviour {

	#region Script's Summary
	//This script is displaying tooltip
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	private static Text tooltip_Text;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	/*
	void Awake (){

	}
	*/

	void Start () {

		//--------- variable definitions -----------

		tooltip_Text = GetComponent<Text> ();

		//-------------------------------------------
	}

	/*
	void Update () {
		
	}
	*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	public static void ShowTooltip(string tip){

		tooltip_Text.text = tip;

		if(tip == ""){
			tooltip_Text.enabled = false;
		}else{
			tooltip_Text.enabled = true;
		}
	}
	//----------------------------------------------------------------------

	#endregion
}
