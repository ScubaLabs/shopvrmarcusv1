﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;

/// <summary>
/// status of the mouse/touch action
/// </summary>
public enum touch_Stages{none, click, double_Click, long_Click, swipe_Up, swipe_Down, swipe_Left, swipe_Right, cancel}

/// <summary>
/// speed of the swipe
/// </summary>
public enum swipe_Speed{none, slow, medium, fast}

public class CameraControl : MonoBehaviour {

	#region Script's Summary
	//This script is responsible for the recticle interaction with the world
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private int ray_Length;				//length of the raycast

	public touch_Stages touch_Status;				//current status of the touch
	public swipe_Speed swipe_Status;					//current status of the swipe

	private bool is_Clicked;								//is mouse button down
	Vector3 click_Start_Position;							//position where the screen is touched
	Vector3 click_Current_Position;							//current position of the touch
	Vector3 click_End_Position;								//position where the screen is left

	private Vector2 swipe_Direction;						//difference of the current position and the start position
	private float swipe_Width;								//width after which a swipe is valid
	private float swipe_Diameter;							//diameter of the touch area

	private float first_Click_End_Time;						//time when click released
	private float double_Click_Time_Scale;					//max time between 2 clicks to be a double click

	private float long_Click_Start_Time;					//time when clicked
	private float long_Click_End_Time;						//time when click released(different from first_Click_End_Time)
	private float long_Click_Time_Scale;					//min time for a click to be a long click

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions ---------

		TouchRefresh ();

		ray_Length = 1000;

		is_Clicked = false;

		click_Start_Position = click_Current_Position = click_End_Position = Vector3.zero;
		swipe_Direction = Vector2.zero;
		first_Click_End_Time = 0;
		double_Click_Time_Scale = 0.8f;
		long_Click_Start_Time = long_Click_End_Time = 0;
		long_Click_Time_Scale = 1f;

		swipe_Diameter = 300f;
		swipe_Width = 20f;

		//-------------------------------------------
	}

	void Update(){

		TouchRefresh ();

		ClickCheck ();

		if ((click_Start_Position == click_End_Position) && (click_Start_Position != Vector3.zero)){

			touch_Status = touch_Stages.click;
			//Debug.Log ("single clicked");

			DoubleClickCheck ();
			LongClickCheck ();
		}
		
		SwipeCheck ();

		if ((click_Current_Position - click_Start_Position != Vector3.zero) && (click_End_Position == Vector3.zero)) {
			//Debug.Log ("user swipe");

			swipe_Direction = new Vector2 (click_Current_Position.x - click_Start_Position.x, click_Current_Position.y - click_Start_Position.y);
			//Debug.Log (swipe_Direction);

			if (swipe_Direction.y > swipe_Width && (swipe_Direction.x > -swipe_Width && swipe_Direction.x < swipe_Width)) {
				//Debug.Log ("swipe up");
				touch_Status = touch_Stages.swipe_Up;
				SetSwipeSpeed (swipe_Direction.y);

			} else if (swipe_Direction.y < -swipe_Width && (swipe_Direction.x > -swipe_Width && swipe_Direction.x < swipe_Width)) {
				//Debug.Log ("swipe down");
				touch_Status = touch_Stages.swipe_Down;
				SetSwipeSpeed (swipe_Direction.y);

			} else if (swipe_Direction.x < -swipe_Width && (swipe_Direction.y > -swipe_Width && swipe_Direction.y < swipe_Width)) {
				//Debug.Log ("swipe left");
				touch_Status = touch_Stages.swipe_Left;
				SetSwipeSpeed (swipe_Direction.x);

			} else if (swipe_Direction.x > swipe_Width && (swipe_Direction.y > -swipe_Width && swipe_Direction.y < swipe_Width)) {
				//Debug.Log ("swipe right");
				touch_Status = touch_Stages.swipe_Right;
				SetSwipeSpeed (swipe_Direction.x);
			} else {
				TouchRefresh ();
			}
		}

		CancelCheck ();
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Raycast at the center of the camera
	/// </summary>
	public RaycastHit CastingTheRay(){

		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;

		Physics.Raycast (ray, out hit, ray_Length);

		return hit; //can return null
	}

	/// <summary>
	/// when an item is clicked pass the data to function
	/// </summary>
	void ClickCheck(){

		if (!is_Clicked) {
			if (Input.GetButtonDown ("Fire1")) {
				is_Clicked = true;
				click_Start_Position = Input.mousePosition;
				long_Click_Start_Time = Time.timeSinceLevelLoad;
			}
		}
		if (Input.GetButtonUp ("Fire1")) {
			is_Clicked = false;
			click_End_Position = Input.mousePosition;
			TouchRefresh ();
			long_Click_End_Time = Time.timeSinceLevelLoad;
		}
	}

	void DoubleClickCheck(){

		if (first_Click_End_Time == 0) {
			first_Click_End_Time = Time.timeSinceLevelLoad;
			return;
		} else if (Time.timeSinceLevelLoad - first_Click_End_Time > double_Click_Time_Scale) {
			first_Click_End_Time = Time.timeSinceLevelLoad;
			return;
		}

		//Debug.Log ("double click");

		touch_Status = touch_Stages.double_Click;
		first_Click_End_Time = 0;

	}

	void LongClickCheck(){

		//Debug.Log("start " + long_Click_Start_Time+ "  end "+ long_Click_End_Time);

		if (long_Click_End_Time - long_Click_Start_Time < long_Click_Time_Scale)
			return;

		//Debug.Log ("Long click");
		touch_Status = touch_Stages.long_Click;

		long_Click_Start_Time = long_Click_End_Time = 0;

	}
	void SwipeCheck(){

		if (is_Clicked) 
			click_Current_Position = Input.mousePosition;

		if(!is_Clicked)
			click_Start_Position = click_Current_Position = click_End_Position = Vector3.zero;
	}

	void CancelCheck(){
		
		if(Input.GetButtonDown("Cancel")){
			touch_Status = touch_Stages.cancel;
		}
	}

	void SetSwipeSpeed(float distance){

		distance = Mathf.Abs (distance);

		if (distance < swipe_Diameter / 3) {
			swipe_Status = swipe_Speed.slow;
		}else if(distance < (swipe_Diameter / 3) * 2){
			swipe_Status = swipe_Speed.medium;
		}else{
			swipe_Status = swipe_Speed.fast;
		}
	}

	void TouchRefresh(){

		touch_Status = touch_Stages.none;
		swipe_Status = swipe_Speed.none;
	}

	//----------------------------------------------------------------------

	#endregion
}