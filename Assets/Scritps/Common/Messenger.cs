﻿using System.Collections;
using UnityEngine;

public class Messenger : MonoBehaviour {

	#region Script's Summary
	//This script is responsible for message passing between scenes
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	public bool has_Category_Data;
	public string[] category_Data;

	public bool has_Shop_Data;
	public string[] shop_Data;

	public string selected_Category;
	public string selected_Shop;

	private static GameObject self_Object;


    public string id;
    public string name;
    public string size;
    public string color;
    public int original_Price;
    public int discount;
    public int current_Price;
    public string available;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    void Awake (){

		if (self_Object == null) {
			self_Object = gameObject;

		} else {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);

		has_Category_Data = false;
		has_Shop_Data = false;

		selected_Category = null;
		selected_Shop = null;
	}

	void Start () {

		//--------- variable definitions -----------

		StartCoroutine (CategoryFetch ());

		//-------------------------------------------
	}

	/*void Update(){

	}*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	IEnumerator CategoryFetch(){

		TextAsset text_File = Resources.Load ("category") as TextAsset;
	
		ProductCommon product_Data = JsonUtility.FromJson<ProductCommon>(text_File.text);

		category_Data = product_Data.category;
		has_Category_Data = true;

		//text_File = Resources.Load ("shop1") as TextAsset;
		//ShopName shop_Data = JsonUtility.FromJson<ShopName>(text_File.text);

		//Shop sd = JsonUtility.FromJson<Shop>(shop_Data.item[0]);
		//Debug.Log(shop_Data.Bracelets);

		yield return null;
	}

	public IEnumerator ShopFetch(){

		string[] str = { "shop1", "shop2", "shop3", "shop4", "shop5", "shop6" };

		shop_Data = str;
		has_Shop_Data = true;

		yield return null;
	}

	//----------------------------------------------------------------------

	#endregion
}

