﻿                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class CameraScene1 : MonoBehaviour {


	#region Script's Summary
	//This script is responsible for the interaction of reticle with the world
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private CenterMenu CenterMenu_Script;		//parent of all the center menu item
	[SerializeField] private VRInput VRInput_Script;			//vr script
	[SerializeField] private GameObject LoadingBar;				//progress bar

	private SelectionRadial SelectionRadial_Script;
	private AudioSource self_Audio;								//audio component of self

	private Messenger messenger_Script;							//this script passes message from one scene to another

	private CameraControl camera_Script;						//input script attached to camera
	private RaycastHit hit;										//raycast target

	private LeftIconBehaviour current_Left_Icon;			//script on a left item menu
	private CenterIconBehaviour current_Center_Icon;			//script on a center item menu 



	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

		QualitySettings.antiAliasing = 2;
	}

	void Start () {

		//--------- variable definitions ---------

		camera_Script = GetComponent<CameraControl> ();
		SelectionRadial_Script = GetComponent<SelectionRadial> ();
		self_Audio = GetComponent<AudioSource> ();
		messenger_Script = GameObject.Find ("Messenger").GetComponent<Messenger> ();

		current_Left_Icon = null;
		current_Center_Icon = null;

		//-------------------------------------------
	}

	void Update () {

		hit = camera_Script.CastingTheRay ();
		ItemHover();
	}

	void OnEnable(){

		VRInput_Script.OnClick += ItemClicked;
	}

	void OnDisable(){

		VRInput_Script.OnClick -= ItemClicked;
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Raycast at the center of the camera
	/// </summary>
	void ItemHover(){

		//if ray is not colliding with any object
		if (!hit.collider)
			return;

		if (hit.collider.CompareTag ("Left Icon")) {

			hit.collider.GetComponentInParent<LeftIconBehaviour> ().HoverControl (hit);
			current_Left_Icon = hit.collider.GetComponentInParent<LeftIconBehaviour> ();

		} else if (hit.collider.CompareTag ("Center Icon")) {

			hit.collider.GetComponentInParent<CenterIconBehaviour> ().IconHoverIn (hit);
			current_Center_Icon = hit.collider.GetComponentInParent<CenterIconBehaviour> ();

		} else if (hit.collider.CompareTag ("Finish")) {

			if (current_Left_Icon) {
				current_Left_Icon.IconHoverOut ();
				current_Left_Icon = null;

			} else if (current_Center_Icon) {
				current_Center_Icon.IconHoverOut ();
				current_Center_Icon = null;
			}
		}
			
		SelectionRadial_Script.Show ();
	}

	/// <summary>
	/// when an item is clicked pass the data to function
	/// </summary>
	void ItemClicked(){

		if (current_Left_Icon) {
			
			self_Audio.Play(); //play the click sound

			messenger_Script.selected_Category = hit.collider.name; //store the name for reference in other scene
			StartCoroutine(messenger_Script.ShopFetch());
			CenterMenu_Script.GetCategories ();
		}

		if ((current_Center_Icon != null) && (messenger_Script.selected_Category != null)) {
			
			messenger_Script.selected_Shop = hit.collider.name;
			StartCoroutine ("LoadNewScene");
		}
	}

	/// <summary>
	/// Loads the new scene with a different thread.
	/// </summary>
	/// <returns>The new scene.</returns>
	IEnumerator LoadNewScene(){

		AsyncOperation scene2 = SceneManager.LoadSceneAsync ("Shop");

		while (!scene2.isDone) {

			if (LoadingBar.activeSelf == false) {
				LoadingBar.SetActive (true);
				//Debug.Log ("active");
			}
			//Debug.Log (scene2.progress);
			yield return 0;
		}

		//Debug.Log ("deactive");
		LoadingBar.SetActive (false);

		yield return null;
	}

	//----------------------------------------------------------------------

	#endregion
}