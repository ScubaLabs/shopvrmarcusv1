﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterIconBehaviour : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the behaviour of the center items
	//when recticle hover them, which property to change
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	private Image item_image;					//image component of the item on which the recticle is pointing
	private Color current_Color;				//current color of the image
	private Color hover_Color;					//color to change when hovered

	private Image icon_Image;					//image component of the item on which the recticle is pointing
	private Color current_Icon_Color;			//current color of the icons
	private Color Increased_Alpha;				//color to produce a glow effect

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	/*void Awake (){

	}*/

	void Start () {

		//--------- variable definitions ---------

		current_Icon_Color = new Color(.102f, .925f, 1f, .396f);
		Increased_Alpha = new Color(current_Icon_Color.r, current_Icon_Color.g, current_Icon_Color.b, 1f);

		//-------------------------------------------
	}

	/*void Update () {

	}*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// reticle is over the icon
	/// </summary>
	public void IconHoverIn(RaycastHit hit){

		icon_Image = hit.collider.GetComponent<Image> ();
		icon_Image.color = Increased_Alpha;
	}

	/// <summary>
	/// reticle is out of the icon
	/// </summary>
	public void IconHoverOut(){

		icon_Image.color = current_Icon_Color;
	}

	/*

	public static Color HexToColor(string hex)
	{
		hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if (hex.Length == 8)
		{
			a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r, g, b, a);
	}*/

	//----------------------------------------------------------------------

	#endregion
}
