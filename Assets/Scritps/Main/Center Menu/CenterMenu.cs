﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterMenu : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the pictures in the center menu
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private GameObject MiddlePanel;	//all the children of the center menu

	private Text[] middlePanel_Children;
	private Messenger messenger_Script;

	//public string selected_Item_Name; 		//this variable will be set by another script
	private List<string> shop_Name;							//name of the shops

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions -----------

		middlePanel_Children = MiddlePanel.GetComponentsInChildren<Text> ();
		messenger_Script = GameObject.Find ("Messenger").GetComponent<Messenger> ();

		shop_Name = new List<string> ();

		//-------------------------------------------
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Gets the list of categories from messenger to display.
	/// </summary>
	public void GetCategories(){

		if (messenger_Script.has_Shop_Data == false) {
			Invoke ("GetCategories", 1f);
			return;
		}

		foreach (string child in messenger_Script.shop_Data) {
			shop_Name.Add (child);
		}

		DisplaySelectedData ();

	}

	/// <summary>
	/// display the selected data from the left menu list
	/// </summary>
	public void DisplaySelectedData(){

		for (int i = 0; i < middlePanel_Children.Length; i++) {
			middlePanel_Children[i].text = shop_Name[i];
		}
	}
	//----------------------------------------------------------------------

	#endregion
}
