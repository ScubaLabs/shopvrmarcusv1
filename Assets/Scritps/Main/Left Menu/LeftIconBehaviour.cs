﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftIconBehaviour : MonoBehaviour {

	#region Script's Summary
	//This script is managing the behaviour of categories item
	//which propertes to change when the reticle is over an item
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	private Image icon_Image;					//image component of the item on which the recticle is pointing
	private Color current_Icon_Color;			//current color of the icons
	private Color Increased_Alpha;				//color to produce a glow effect

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	/*void Awake (){

	}*/

	void Start () {

		//--------- variable definitions ---------

		current_Icon_Color = new Color(.102f, .925f, 1f, .396f);
		Increased_Alpha = new Color(current_Icon_Color.r, current_Icon_Color.g, current_Icon_Color.b, 1f);

		//-------------------------------------------
	}

	/*void Update () {

	}*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// control all the hover function
	/// </summary>
	/// <param name="is_Hovered">If set to <c>true</c> reticle is over the icon.</param>
	/// <param name="is_Clicked">If set to <c>true</c> icon clicked.</param>
	public void HoverControl(RaycastHit hit){

		icon_Image = hit.collider.GetComponent<Image> ();

		IconHoverIn ();
	}

	/// <summary>
	/// reticle is over the icon
	/// </summary>
	void IconHoverIn(){
		
		icon_Image.color = Increased_Alpha;
	}

	/// <summary>
	/// reticle is out of the icon
	/// </summary>
	public void IconHoverOut(){

		icon_Image.color = current_Icon_Color;
	}

	//----------------------------------------------------------------------

	#endregion
}