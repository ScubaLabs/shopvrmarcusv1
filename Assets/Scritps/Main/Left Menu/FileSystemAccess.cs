﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileSystemAccess : MonoBehaviour {

	#region Script's Summary
	//This script is 
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	private GameObject temp;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions ---------

		demo ();

		//-------------------------------------------
	}

	/*void Update () {

	}*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	void demo(){

		string folder_Name;	//actual name of the folder
		//string file_Name;	//actual name of the file
		string filtered_Folder_Name; //name to be displayed
		char[] name;	//temp array

		DirectoryInfo current_Path = new DirectoryInfo (Application.dataPath + @"\Resources");

		DirectoryInfo[] di = current_Path.GetDirectories();

		foreach (DirectoryInfo file in di) {

			folder_Name = file.Name;

			if(folder_Name.Contains("model")){
				Debug.Log ("model " + folder_Name);
				return;
			}

			folder_Name = folder_Name.Trim (); //remove all the spaces from front and back
			folder_Name = folder_Name.ToLower (); //convert the string to lower case

			name = folder_Name.ToCharArray(); 
			name[0] = (char)((int)name [0] - 32); //make the first letter capital

			filtered_Folder_Name = ArrayToString (name);

			Debug.Log (filtered_Folder_Name);
		}

	}

	/// <summary>
	/// char array to string
	/// </summary>
	/// <returns>string.</returns>
	/// <param name="c">char array</param>
	string ArrayToString(char[] c){

		string temp = null;

		foreach (char value in c) {
			temp += value.ToString ();
		}
		return temp;
	}

	//----------------------------------------------------------------------

	#endregion
}
