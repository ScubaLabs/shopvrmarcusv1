﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class LeftMenu : MonoBehaviour {

	#region Script's Summary
	//This script is managing the left menu and all the data to display
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[Tooltip("drag the 'Small Tile' from the prefab hierarchy")]
	[SerializeField] private GameObject small_Tile;			//prefab of visual properties of an item

	[Tooltip("drag the 'Large Tile' from the prefab hierarchy")]
	[SerializeField] private GameObject large_Tile;			//prefab of visual properties of an item

	[Tooltip("drag the 'Tile Holder' from the prefab hierarchy")]
	[SerializeField] private GameObject tile_Holder;		//hold the tile in every row

	[Tooltip("drag the 'Content' from the left hierarchy")]
	[SerializeField] private GameObject content;			//parent of all the generated prefab

	private List<string> item_Name;							//name of the categories
	private Messenger messenger_Script;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions ---------

		item_Name = new List<string> ();
		messenger_Script = GameObject.Find ("Messenger").GetComponent<Messenger> ();

		GetCategories ();

		//-------------------------------------------
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Gets the list of categories from messenger to display.
	/// </summary>
	void GetCategories(){

		//if no data then invoke after 1 sec
		if (messenger_Script.has_Category_Data == false) {
			Invoke ("GetCategories", 1f);
			return;
		}
			
		foreach (string child in messenger_Script.category_Data) {
			item_Name.Add (child);
		}

		GenerateCategory ();
	}

	/// <summary>
	/// Generates the list of categories
	/// </summary>
	void GenerateCategory(){

		GameObject temp_Tile_Holder = null;
		GameObject temp_Tile = null;

		int temp_flag = 0;

		foreach (string child in item_Name ){

			//after every two item generate a tile holder
			if(temp_flag == 0){
				
				temp_Tile_Holder = Instantiate (tile_Holder, transform.position, Quaternion.identity) as GameObject; //instantiate an item holder
				temp_Tile_Holder.transform.SetParent (content.transform); //set its parent
				temp_Tile_Holder.transform.rotation = new Quaternion (); //set its rotation
			}

			temp_Tile = Instantiate(small_Tile, transform.position, Quaternion.identity) as GameObject; //instantiate a list item
			temp_Tile.transform.SetParent (temp_Tile_Holder.transform); //set its parent
			temp_Tile.transform.rotation = new Quaternion (); //set its rotation

			temp_Tile.name = child; //set a name for the instantiated
			temp_Tile.GetComponentInChildren<Text>().text = child; //set a name to display

			temp_flag++;

			if (temp_flag == 2)
				temp_flag = 0;
		}
	}

	//----------------------------------------------------------------------

	#endregion
}
