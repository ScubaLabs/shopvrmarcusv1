﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPayement : MonoBehaviour {

    #region Script's Summary
    //This script is Controlling the Camera
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    private CameraControl cam_Control;
    private RaycastHit hit;

    public Checkout checkout_Script;
    public Cart cart_Script;
    public Success success_Script;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        cam_Control = GetComponent<CameraControl>();

        //-------------------------------------------
    }

	void Update () {

        if(cam_Control.touch_Status == touch_Stages.click || cam_Control.touch_Status == touch_Stages.double_Click)
        {
            hit = cam_Control.CastingTheRay();
            if (hit.collider == null)
            {
                if(checkout_Script.is_Active == true)
                    checkout_Script.DisableKeyboard();

                return;
            }

            //user is interacting with keyboard or with quantity buttons
            if (hit.collider.CompareTag("Icon"))
            {
                //user is interacting with the quantity buttons
                if(checkout_Script.is_Active == false)
                    hit.collider.GetComponentInParent<Quantity>().QuantityButtonController(hit);

                if (success_Script.is_Active == true)
                    success_Script.ButtonPressed();

                return;
            }

            if (checkout_Script.is_Active == true)
            {
                checkout_Script.CheckoutElement(hit);
            }else
            {
                cart_Script.ButtonPressed(hit);
            }

        }
	}

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------



    //----------------------------------------------------------------------

    #endregion
}