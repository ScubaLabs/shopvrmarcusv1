﻿using UnityEngine;
using UnityEngine.UI;
using CurvedVRKeyboard;

public class Checkout : MonoBehaviour {

    #region Script's Summary
    //This script is Controlling the Checkout elements
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject[] child_Elements;     //contains all the interactable element of the checkout page
    public GameObject pay_Button;           //payement button
    public GameObject Keyboard;

    public GameObject success;

    public bool is_Active;

    private Text selected_Field;
    private string field_String;


    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        is_Active = true;
        gameObject.SetActive(is_Active);
        Keyboard.SetActive(false);

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

        /// <summary>
        /// Find which element is clicked
        /// </summary>
        /// <param name="hit"></param>
    public void CheckoutElement(RaycastHit hit)
    {
        if (selected_Field != null)
            DisableKeyboard();
        
       foreach(GameObject child in child_Elements)
        {
            if(child.name == hit.collider.name)
            {
                Keyboard.SetActive(true);

                selected_Field = child.GetComponentInChildren<Text>();
                field_String = (string)selected_Field.text;
                selected_Field.text = "";
                selected_Field.color = Color.black;

                Keyboard.GetComponent<KeyboardStatus>().output = selected_Field;
            }
        }

      if(pay_Button.name == hit.collider.name)
        {
            PayementProcess();
        }
    }

    public void DisableKeyboard()
    {
        if(selected_Field.text == "")
        {
            selected_Field.text = field_String;
            selected_Field.color = new Color(0.68f, 0.68f, 0.68f);
        }

        Keyboard.SetActive(false);
        Keyboard.GetComponent<KeyboardStatus>().output = null;
        selected_Field = null;
    }

    private void PayementProcess()
    {
        Success s = success.GetComponent<Success>();

        s.card_Num = child_Elements[0].GetComponentInChildren<Text>().text;
        s.email = child_Elements[3].GetComponentInChildren<Text>().text;
        s.is_Active = true;

        success.SetActive(true);
        
    }

    //----------------------------------------------------------------------

    #endregion
}