﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Success : MonoBehaviour {

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject[] details;

    public bool is_Active = false;

    public string card_Num;
    public string email;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------
        
        details[0].GetComponent<Text>().text = card_Num;
        details[1].GetComponent<Text>().text = email;

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void ButtonPressed()
    {
        SceneManager.LoadScene("Main");
        Debug.Log("load");
    }

    //----------------------------------------------------------------------

    #endregion
}

