﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Cart : MonoBehaviour
{

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    [Tooltip("drag the 'content'")]
    public GameObject content;
    [Tooltip("drag the 'row' prefab")]
    public GameObject row;

    [Tooltip("drag the element to display total amount")]
    public Text total_Amount;                   //total amount 
    public GameObject[] child_Button;           //all the interactable buttons

    public GameObject checkout;

    private Quantity[] content_Total;           //all child of the content
    private float amount;

    private Messenger mesenger_Script;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        content_Total = content.GetComponentsInChildren<Quantity>();
        TotalPriceChange();
        mesenger_Script = GameObject.Find("Messenger").GetComponent<Messenger>();

        ItemGenerator();

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void TotalPriceChange()
    {
        amount = 0f;
        foreach (Quantity child in content_Total)
        {
            amount += float.Parse(child.total_Price.text.ToString());
        }
        total_Amount.text = amount.ToString() + ".00";
    }

    public void ButtonPressed(RaycastHit hit)
    {
        //checkout
        if (hit.collider.name == child_Button[1].name)
        {
            checkout.SetActive(true);
            checkout.GetComponent<Checkout>().is_Active = true;
            checkout.GetComponent<Checkout>().pay_Button.GetComponentInChildren<Text>().text = 
                "Pay " + amount.ToString() +"$";
        }
        else
        {
            SceneManager.LoadScene("Shop");
        }
        
    }

    void ItemGenerator()
    {
        GameObject item = Instantiate(row, content.transform.parent, true) as GameObject;
        item.transform.GetChild(1).GetComponent<Text>().text = mesenger_Script.name;
        item.transform.GetChild(2).GetComponent<Text>().text = mesenger_Script.current_Price.ToString();
    }

    //----------------------------------------------------------------------

    #endregion
}