﻿using UnityEngine;
using UnityEngine.UI;

public class Quantity : MonoBehaviour
{

    #region Script's Summary
    //This script is Controlling the Quantity and price variation
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

        [Tooltip("drag the 'cart'")]
    public Cart cart_Script;                //cart script to change total amount
        [Tooltip("drag the 'up' button, then the 'down' button")]
    public GameObject[] quantity_Buttons;   //both the up and down button to change the quantity
        [Tooltip("drag the text to display quantity")]
    public Text quantity_Count;             //text which display the quantity

    public Text price;                      //text which display the price of an item
    public Text total_Price;                //text which display the total price of an item

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------



        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void QuantityButtonController(RaycastHit hit)
    {
        //parsing string to int
        int count = int.Parse(quantity_Count.text.ToString());
        float amount = float.Parse(price.text.ToString());
        //up button is pressed
        if (quantity_Buttons[0].name == hit.collider.name)
        {
            count++;
        }
        else //down buton is pressed
        {
            if(count > 1)
                count--;
        }

        quantity_Count.text = count.ToString();
        total_Price.text = (amount * count).ToString() + ".00";
        cart_Script.TotalPriceChange();
    }

    //----------------------------------------------------------------------

    #endregion
}