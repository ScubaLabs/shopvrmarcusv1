﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailPane : MonoBehaviour {
    #region Script's Summary
    //This script is controlling the details cube
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject close_View;

    private string id;
    public Text name;
    public Text size;
    public Text color;
    public Text original_Price;
    public Text discount;
    public Text current_Price;
    public Text available;

    private ProductStructure product_Script;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    void Start () {

		//--------- variable definitions ------------



		//-------------------------------------------
	}

	void Update () {

		transform.Rotate (0, 10 * Time.deltaTime, 0);

	}

    private void OnEnable()
    {
        product_Script = close_View.GetComponentInChildren<ProductStructure>();

        id = product_Script.id;
        name.text = product_Script.name;
        size.text = product_Script.size;
        color.text = product_Script.color;
        original_Price.text = product_Script.original_price.ToString() + " $";
        discount.text = product_Script.discount.ToString() + " %";
        current_Price.text = product_Script.current_price.ToString() + " $";
        available.text = product_Script.available;
    }

    private void OnDisable()
    {
        transform.rotation = Quaternion.Euler(0,0,0);
    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------



    //----------------------------------------------------------------------

    #endregion
}
