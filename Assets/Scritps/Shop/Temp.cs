﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Temp : MonoBehaviour {

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject close_View;

    private Messenger mesenger_Script;
    private ProductStructure product_Script;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/
    private void OnEnable()
    {
        product_Script = close_View.GetComponentInChildren<ProductStructure>();
        mesenger_Script = GameObject.Find("Messenger").GetComponent<Messenger>();
        
        mesenger_Script.id = product_Script.id;
        mesenger_Script.name = product_Script.name;
        mesenger_Script.size = product_Script.size;
        mesenger_Script.color = product_Script.color;
        mesenger_Script.original_Price = product_Script.original_price;
        mesenger_Script.discount = product_Script.discount;
        mesenger_Script.current_Price = product_Script.current_price;
        mesenger_Script.available = product_Script.available;

       SceneManager.LoadScene("Payement");
    }
    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------



    //----------------------------------------------------------------------

    #endregion
}
