﻿using System.Collections;
using UnityEngine;

public class CloseView : MonoBehaviour {

	#region Script's Summary
	//This script is responsible for the control of product after its selection
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private GameObject options;			//enable it after selection
	[SerializeField] private Transform moved_Product;

	private Transform selected_Object_Transform;			//selected product
	private Transform Object_Parent_Transform;				//parent(spawn point) of the selected product

    private float rotation_Speed;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    void Start () {

		//--------- variable definitions ---------

		selected_Object_Transform = null;
		Object_Parent_Transform = null;

        rotation_Speed = 10f;

        //-------------------------------------------
    }

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="direction">swipe direction.</param>
	public void OnHover(string direction = ""){

		if (direction == "up") {
			transform.RotateAround (transform.position, Vector3.left, rotation_Speed);
		}else if (direction == "down") {
			transform.RotateAround (transform.position, Vector3.right, rotation_Speed); 
		}else if (direction == "left") {
			transform.RotateAround (transform.position, Vector3.up, rotation_Speed); 
		}else if (direction == "right") {
			transform.RotateAround (transform.position, Vector3.down, rotation_Speed); 
		}
	}
		
	public void OnClicked(RaycastHit hit, bool product_Movement = false){

		if(product_Movement){
			ProductMovement (hit);
			return;
		}

		//if selected item is clicked again then return
		if (selected_Object_Transform == hit.collider.transform)
			return;
		if (selected_Object_Transform != null)
			ItemReturn ();


        //if the clicked product has no id then call the function to fetch data
        if (hit.collider.GetComponent<ProductStructure>().id == null)
           // StartCoroutine(FetchData(hit.collider.transform.parent.name, hit.collider.name, hit.collider.GetComponent<ProductStructure>()));

        //change the parent of the product
        selected_Object_Transform = hit.collider.transform;
		Object_Parent_Transform = hit.collider.transform.parent;

		hit.collider.transform.SetParent (transform);
		hit.collider.transform.localPosition = Vector3.zero;
        
		//show the options
		options.SetActive (true);
	}

	/// <summary>
	/// Return the product to its original position
	/// </summary>
	public bool ItemReturn(){

		if (selected_Object_Transform == null)
			return true;	//no item

		//when returning the item clear the display of the previous item
		//options.GetComponent<IconsBehaviour> ().DisplayReset ();

		selected_Object_Transform.SetParent (Object_Parent_Transform);
		selected_Object_Transform.localPosition = Vector3.zero;
		selected_Object_Transform.localRotation = new Quaternion ();

		selected_Object_Transform = null;
		Object_Parent_Transform = null;

		foreach (IconBehaviour child in options.GetComponentsInChildren<IconBehaviour>()) {
			child.IconReset ();
		}
		options.SetActive (false);

		//item placed back 
		return false;
	}


	/// <summary>
	/// Movement of the product
	/// </summary>
	public void ProductMovement(RaycastHit hit){
		
		selected_Object_Transform.position = hit.point;

		selected_Object_Transform.SetParent (moved_Product);

		selected_Object_Transform = null;
		Object_Parent_Transform = null;

		foreach (IconBehaviour child in options.GetComponentsInChildren<IconBehaviour>()) {
			child.IconReset ();
		}
		options.SetActive (false);
	}


    IEnumerator FetchData(string name, string id, ProductStructure product_Script)
    {
        //fetch the data of the product
        FetchProductData fpd= new FetchProductData();

        fpd.ProductData();                          //....................................//TODO refactor later
        fpd.GetProductData(name, id, product_Script);

        yield return 0;
    }
	//----------------------------------------------------------------------

	#endregion
}
