﻿using System.Collections;
using UnityEngine;
using VRStandardAssets.Utils;

// This script flips through a series of textures
// whilst the user is looking at it.
public class MovieAnimator : MonoBehaviour
{
    [SerializeField]
    private int m_FrameRate = 30;                  // The number of times per second the image should change.
    [SerializeField]
    private MeshRenderer m_ScreenMesh;             // The mesh renderer who's texture will be changed.
    [SerializeField]
    private Texture[] m_AnimTextures;              // The textures that will be looped through.
	[SerializeField]
	private AudioSource m_Audio;

    private WaitForSeconds m_FrameRateWait;                         // The delay between frames.
    private int m_CurrentTextureIndex;                              // The index of the textures array.
    //private bool m_Playing;                                         // Whether the textures are currently being looped through.



    private void OnEnable()
    {
		m_AnimTextures = Resources.LoadAll<Texture> ("Video");
        // The delay between frames is the number of seconds (one) divided by the number of frames that should play during those seconds (frame rate).
        m_FrameRateWait = new WaitForSeconds(1f / m_FrameRate);
        //m_Playing = true;
        StartCoroutine(PlayTextures());
    }

	private void OnDisable(){
		StopCoroutine ("PlayTextures");
		m_CurrentTextureIndex = 0;
	}

    private IEnumerator PlayTextures()
    {
		m_Audio.Play ();

        // So long as the textures should be playing...
		while (m_CurrentTextureIndex < m_AnimTextures.Length-1)
        {
            // Set the texture of the mesh renderer to the texture indicated by the index of the textures array.
            m_ScreenMesh.material.mainTexture = m_AnimTextures[m_CurrentTextureIndex];

            // Then increment the texture index (looping once it reaches the length of the textures array.
            //m_CurrentTextureIndex = (m_CurrentTextureIndex + 1) % m_AnimTextures.Length;
			m_CurrentTextureIndex++;

			//Debug.Log (m_AnimTextures[m_CurrentTextureIndex]);
            // Wait for the next frame.
            yield return m_FrameRateWait;

        }
		m_CurrentTextureIndex = 0;
		StartCoroutine(PlayTextures());
    }
}