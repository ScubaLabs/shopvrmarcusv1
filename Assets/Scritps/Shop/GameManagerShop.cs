﻿using System.Collections;
using UnityEngine;
using System.IO;
using UnityEditor;

public class GameManagerShop : MonoBehaviour {

	#region Script's Summary
	//This script is spawning the Products
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[Tooltip("Drag the 'main Spawn Points' from the left hierarchy")]
	public GameObject main_Spawn_Points;					//parents of all spawn points
	[Tooltip("Drag the 'Spawn Points' from the left hierarchy")]
	public GameObject other_Spawn_Points;					//parents of all spawn points

	private Transform[] spawn_Point_Child;					//all spawn points of parent

	private Messenger messenger_Script;						//script which passes messages between scenes
	private string selected_Item;							//currently selected item

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------


	void Start () {

		//--------- variable definitions ------------

		//this script passes message from one scene to another
		messenger_Script = GameObject.Find ("Messenger").GetComponent<Messenger> ();

		//currently selected item
		selected_Item = messenger_Script.selected_Category;

		if (selected_Item != null)
			StartCoroutine (ItemSpawner ());

		//-------------------------------------------
	}

	/*void Update () {

	}*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// spawn all the products 
	/// </summary>
	IEnumerator ItemSpawner(){

		GameObject temp;
		GameObject temp_Object;

        string[] all_Prefabs = GetFileNames(selected_Item);

        //GameObject[] temp_object_arr = Resources.LoadAll<GameObject>(selected_Item + " Model");
        //ResourceRequest rr = Resources.LoadAsync(selected_Item + " Model");
        spawn_Point_Child = main_Spawn_Points.transform.GetComponentsInChildren<Transform> ();
        
       for(int i = 1; i < spawn_Point_Child.Length; i++) {
			
			//temp_Object = (GameObject) Resources.LoadAsync<GameObject>(selected_Item + " Model/0" + i.ToString()).asset;
            temp_Object = (GameObject)AssetDatabase.LoadAssetAtPath(all_Prefabs[i-1], typeof(GameObject));

            temp = Instantiate (temp_Object, spawn_Point_Child[i].position, temp_Object.transform.rotation) as GameObject;

			temp.transform.SetParent (spawn_Point_Child [i]);
			temp.name = "0" + i.ToString ();
            temp.transform.parent.name = selected_Item;

            temp.AddComponent(typeof(ProductStructure));

            //fetch the data of the product
            FetchProductData fpd = new FetchProductData();

            fpd.ProductData();                          //....................................//TODO refactor later
            fpd.GetProductData(selected_Item, temp.name, temp.GetComponent<ProductStructure>());
        }

       selected_Item = (selected_Item == "Shirts") ? "Shoes" : "Shirts";

		spawn_Point_Child = other_Spawn_Points.transform.GetComponentsInChildren<Transform> ();
        all_Prefabs = GetFileNames(selected_Item);

        for (int i = 1; i < spawn_Point_Child.Length; i++){

            //temp_Object = (GameObject) Resources.LoadAsync<GameObject>(selected_Item + " Model/0" + i.ToString()).asset;
            temp_Object = (GameObject)AssetDatabase.LoadAssetAtPath(all_Prefabs[i - 1], typeof(GameObject));

            temp = Instantiate (temp_Object, spawn_Point_Child[i].position, temp_Object.transform.rotation) as GameObject;

			temp.transform.SetParent (spawn_Point_Child [i]);
			temp.name = "0" + i.ToString ();
            temp.transform.parent.name = selected_Item;

            temp.AddComponent(typeof(ProductStructure));
        }

		yield return null;
	}
		
    string[] GetFileNames(string name)
    {
        DirectoryInfo path = new DirectoryInfo(Application.dataPath + @"/Resources/" + name + " Model");
        FileInfo[] files = path.GetFiles();

        string[] all_Prefabs = new string[5];
        int count = 0;

        foreach(FileInfo child in files)
        {
            //Debug.Log(child.Name + " " + child.Extension);
            if(child.Extension == ".prefab")
            {
                char[] arr = child.FullName.ToCharArray();
                all_Prefabs[count] = BrokeString(arr);
                count++;
            }
        }

        return all_Prefabs;
    }

    /// <summary>
    /// get the substring from a string
    /// </summary>
    /// <param name="arr">original string in array format</param>
    string BrokeString(char[] arr)
    {
        char[] search = {'A', 's', 's', 'e', 't', 's'};
        int start = 0;
        int found_Index = 0;
        bool f = false;

        for(int i=0; i<arr.Length; i++)
        {
            if (arr[i] == search[start])
            {
                start++;
                for (int j = i+1; j < (i+search.Length); j++)
                {
                    found_Index = j - search.Length;
                    if(arr[j] != search[start])
                    {
                        found_Index = 0;
                        start = 0;
                        break;
                    }
                    start++;
                }
                if (start == search.Length)
                    break;
            }
        }
        string substr = "";
        for (int i = found_Index+1; i < arr.Length; i++)
        {
            substr += arr[i].ToString();
        }
        return substr;
    }
	//----------------------------------------------------------------------

	#endregion
}
