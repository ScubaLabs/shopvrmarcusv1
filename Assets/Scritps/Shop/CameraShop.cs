﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class CameraShop : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the shop scene
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private VRInput vr_Script;				//vr component
	[SerializeField] private Image self_Reticle;			//center of reticle
	[SerializeField] private VRCameraFade vr_Camera_Fade;

	[SerializeField] private GameObject close_View;			//hold the product after selection
	[SerializeField] private NavMeshAgent navmesh_Agent;	//navmesh agent for movement
	[SerializeField] private Text tooltip_Text;				//tooltip display component
	[SerializeField] private Transform player;

	private CameraControl camera_Script;					//self made VR API
	private RaycastHit hit;									//current target

	private bool is_Any_Item;								//is any item selected
	private IconBehaviour current_Icon;						//is any icon selected

	private Messenger mesenger_Script;

	private bool product_Movement;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions -------------

		camera_Script = GetComponent<CameraControl> ();
		mesenger_Script = GameObject.Find ("Messenger").GetComponent<Messenger> ();

		is_Any_Item = false;
		current_Icon = null;
		product_Movement = false;

		//-------------------------------------------
	}

	void Update () {

		hit = camera_Script.CastingTheRay ();
		CameraHover ();

		if (camera_Script.touch_Status == touch_Stages.cancel)
			CancelClick ();
	}

	void OnEnable(){

		vr_Script.OnClick += SingleClick;
	}

	void OnDisable(){

		vr_Script.OnClick -= SingleClick;
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// reticle on an object
	/// </summary>
	void CameraHover(){

		//if reticle is over any product
		if (is_Any_Item) {
			if (camera_Script.touch_Status == touch_Stages.swipe_Up) {
				close_View.GetComponent<CloseView> ().OnHover("up");
			} else if (camera_Script.touch_Status == touch_Stages.swipe_Down) {
				close_View.GetComponent<CloseView> ().OnHover("down");
			}else if (camera_Script.touch_Status == touch_Stages.swipe_Left) {
				close_View.GetComponent<CloseView> ().OnHover("left");
			}else if (camera_Script.touch_Status == touch_Stages.swipe_Right) {
				close_View.GetComponent<CloseView> ().OnHover("right");
			}
		}
			
		if (hit.collider == null) {
			return;
		}
		
		//if reticle is over any option
		if (hit.collider.CompareTag ("Icon")) {
			hit.collider.GetComponent<IconBehaviour> ().HoverControl (true, false);
			current_Icon = hit.collider.GetComponent<IconBehaviour> ();
		}else if(hit.collider.CompareTag("Finish") && current_Icon){
			current_Icon.HoverControl (false, false);
			current_Icon = null;
		}
	}

	/// <summary>
	/// click is pressed
	/// </summary>
	void SingleClick(){

		if (hit.collider == null)
			return;

		//ShowTooltip (hit.collider.tag);

		if(product_Movement){
			close_View.GetComponent<CloseView> ().OnClicked (hit, true);
			product_Movement = false;
			return;
		}

		if (hit.collider.CompareTag ("Object")) {
			close_View.GetComponent<CloseView> ().OnClicked (hit);
			is_Any_Item = true;
			return;
		}

		//if reticle is over any option
		if (hit.collider.CompareTag ("Icon")) {
			hit.collider.GetComponent<IconBehaviour>(). HoverControl(true, true);
			if (hit.collider.name == "Move")
				product_Movement = true;
			
			return;
		}

		PlayerNavigation ();

	}

	/// <summary>
	/// cancel button is pressed
	/// </summary>
	void CancelClick(){

		//if any item is selected then return the item
		if (!(is_Any_Item = close_View.GetComponent<CloseView> ().ItemReturn ())) {
			product_Movement = false;
			return;
		}

		mesenger_Script.selected_Category = null;
		mesenger_Script.selected_Shop = null;
		SceneManager.LoadScene ("Main");
	}

	/// <summary>
	/// Movement of the player in the scene.
	/// </summary>
	void PlayerNavigation(){

		if (navmesh_Agent.CalculatePath (hit.point, navmesh_Agent.path)) {
			vr_Camera_Fade.FadeIn (1f, false);
			navmesh_Agent.Warp(hit.point);
		}
	}
		
	//----------------------------------------------------------------------

	#endregion
}
