﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBehaviour : MonoBehaviour {

	#region Script's Summary
	//This script is behaviour of the icons
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[Tooltip("drag the component to enable/disable it from the left hierarchy")]
	[SerializeField] private GameObject display;			//if any

	public string tooltip_Display;							//string to be displayed as tooltip

	private Color current_Icon_Color;						//current color of the icons
	private Color Increased_Alpha;							//color to produce a glow effect

	private bool is_Clicked;								//if icon is clicked

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Start () {

		//--------- variable definitions ---------

		current_Icon_Color = GetComponent<Image>().color;
		Increased_Alpha = new Color(current_Icon_Color.r, current_Icon_Color.g, current_Icon_Color.b, 180f);

		is_Clicked = false;

		//-------------------------------------------
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// control all the hover function
	/// </summary>
	/// <param name="is_Hovered">If set to <c>true</c> reticle is over the icon.</param>
	/// <param name="is_Clicked">If set to <c>true</c> icon clicked.</param>
	public void HoverControl(bool is_Hovered, bool is_Clicked){

		if (is_Hovered && !is_Clicked) {
			IconHoverIn ();
		} else if(is_Hovered && is_Clicked){
			IconClicked ();
		}else{
			IconHoverOut ();
		}
	}
		
	/// <summary>
	/// reset everything to normal
	/// </summary>
	public void IconReset(){

		is_Clicked = false;

		IconHoverOut ();

		if (display) {
			display.SetActive (false);
		}
	}

	/// <summary>
	/// reticle is over the icon
	/// </summary>
	void IconHoverIn(){

		GetComponent<Image> ().color = Increased_Alpha;
		GetComponent<Shadow> ().enabled = true;
		TooltipText.ShowTooltip (tooltip_Display);
		//Debug.Log("hover");
	}

	/// <summary>
	/// reticle is out of the icon
	/// </summary>
	void IconHoverOut(){

		if(!is_Clicked)
			GetComponent<Image> ().color = current_Icon_Color;
		
		GetComponent<Shadow> ().enabled = false;
		TooltipText.ShowTooltip ("");
		//Debug.Log("hoverout");
	}

	/// <summary>
	/// on every click it will switch the state
	/// </summary>
	void IconClicked(){

		is_Clicked = !is_Clicked;

		if (is_Clicked) {
			GetComponent<Image> ().color = Increased_Alpha;
			if (display) {
				display.SetActive (true);
			}
		} else {
			GetComponent<Image> ().color = current_Icon_Color;
			if (display) {
				display.SetActive (false);
			}
		}
	}

	//----------------------------------------------------------------------

	#endregion
}
