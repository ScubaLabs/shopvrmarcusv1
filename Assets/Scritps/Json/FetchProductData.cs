﻿using UnityEngine;
using LitJson;
using System.IO;



public class FetchProductData {

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    private JsonData json_Data;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        ProductData();

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void ProductData()
    {
        string data = File.ReadAllText(Application.dataPath + @"/Resources/data.json");
        json_Data = JsonMapper.ToObject(data);
    }

    public void GetProductData(string name, string id, ProductStructure product_Script)
    {
        for (int i = 0; i < json_Data[name].Count; i++)
        {
            if (json_Data[name][i]["id"].ToString() == id)
            {
                product_Script.id = json_Data[name][i]["id"].ToString();
                product_Script.name = json_Data[name][i]["name"].ToString();
                product_Script.color = json_Data[name][i]["color"].ToString();
                product_Script.size = json_Data[name][i]["size"].ToString();
                product_Script.original_price = (int)json_Data[name][i]["original_price"];
                product_Script.discount = (int)json_Data[name][i]["discount"];
                product_Script.current_price = (int)json_Data[name][i]["current_price"];
                product_Script.available = (string)json_Data[name][i]["available"];
            }
        }
    }

    //----------------------------------------------------------------------

    #endregion
}
