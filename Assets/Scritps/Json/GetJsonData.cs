﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using LitJson;

public class GetJsonData : MonoBehaviour {

	#region Script's Summary
	//This script is Transferring data between server and application
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	string password = "123"; 		//merchant password

	string session_ID = null;		//session id


	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	/*
	void Awake (){

	}
	*/

	void Start () {

		//--------- variable definitions -----------

		StartCoroutine (GetSessionID ());

		//-------------------------------------------
	}

	/*
	void Update () {
		
	}
	*/

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// retreive the session id from the server
	/// </summary>
	IEnumerator GetSessionID (){

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("X-Oc-Merchant-Id", password);
		string url = "newapi2.opencart-api.com:80/api/rest/session";

		// Add a custom header to the request.
		// In this case a basic authentication to access a password protected resource.
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes("username:password"));

		// Post a request to an URL with our custom headers
		WWW www = new WWW(url, null, headers);
		yield return www;
		JsonData jstr = JsonMapper.ToObject (www.text);
		//Debug.Log (jstr ["data"] ["session"]);
		session_ID = (string) jstr ["data"] ["session"]; //store the session id in a variable
		Debug.Log (session_ID);
		StartCoroutine (AddItemInCart ());
	}

	IEnumerator GetCartItems (){

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("X-Oc-Merchant-Id", password);
		headers.Add("X-Oc-Session", session_ID);
		string url = "newapi2.opencart-api.com:80/api/rest/cart";

		// Add a custom header to the request.
		// In this case a basic authentication to access a password protected resource.
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes("username:password"));

		// Post a request to an URL with our custom headers
		WWW www = new WWW(url, null, headers);
		yield return www;
		//JsonData jstr = JsonMapper.ToObject (www.text);
		Debug.Log (www.text);

	}

	IEnumerator AddItemInCart(){

		//int product_id = 34;
		//int quantity = 1;
			
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("X-Oc-Merchant-Id", password);
		headers.Add("X-Oc-Session", session_ID);
		headers.Add("Cart item object", "d");
		string url = "newapi2.opencart-api.com:80/api/rest/cart";

		// Add a custom header to the request.
		// In this case a basic authentication to access a password protected resource.
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes("username:password"));

		// Post a request to an URL with our custom headers
		WWW www = new WWW(url, null, headers);
		yield return www;
		//JsonData jstr = JsonMapper.ToObject (www.text);
		Debug.Log (www.text);

		StartCoroutine (GetCartItems ());
	}

	void example(){
		/*string url = "newapi2.opencart-api.com:80/api/rest/session";
		WWW www = new WWW(url);

		yield return www;

		if (www.error == null) {
			Debug.Log (www.text);
			JsonData jstr = JsonMapper.ToObject (www.text);
			Debug.Log (www.responseHeaders);
			foreach(KeyValuePair<string, string> entry in www.responseHeaders) {
				//Debug.Log(entry.Value + "=" + entry.Key);
			}
		} else {
			Debug.Log (www.error);
		}
		yield break;*/
	}

	//----------------------------------------------------------------------

	#endregion
}
