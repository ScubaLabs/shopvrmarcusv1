﻿
using UnityEngine;

public class ProductStructure:MonoBehaviour{

    public string id = null;
    public string name;
    public string color;
    public string size;
    public int original_price;
    public int discount;
    public int current_price;
    public string available;
}
